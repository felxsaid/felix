<!DOCTYPE html>
<html>
<head>
	<title>First Application</title>
	<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div id="wrapper">
	<form method="post">
		<table border="0">
			<tr>
				<td>Employee Name: </td>
				<td><input type="text" name="empname"></td>
			</tr>

			<tr>
				<td>Employee Email: </td>
				<td><input type="text" name="empemail"></td>
			</tr>

			<tr>
				<td>Employee DOB: </td>
				<td><input type="text" name="empdob"></td>
			</tr>

			<tr>
				<td>Employee Department: </td>
				<td><input type="text" name="empdept"></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" name="save" value="Save Details"></td>
			</tr>
	</table>
	</form>

	<?php

		if(isset($_POST['save'])){
			include 'includes/connection.php';


			$empname = $_POST['empname'];
			$empemail = $_POST['empemail'];
			$empdob = $_POST['empdob'];
			$empdept = $_POST['empdept'];

			$query = ("insert into employees (empname,empemail,empdob,empdept) values ('$empname','$empemail','$empdob','$empdept')");
			$result = $conn->query;

			if($result == TRUE){
				echo 'Record was successfully saved';
			} else{
				echo "Failed" . $conn->connect_error);
			}
		}	
	?>

	<hr>
	<br><br>

	<table border="1" width="100%">
		<thead>
			<tr>
				<th>Employee Name</th>
				<th>Email Address</th>
				<th>Date of Birth</th>
				<th>Department</th>
				<th>&nbsp</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$select = ("select * from employees");
				$res = $conn->query;

				$row_count = $res->num_rows;
				if($row_count > 0){
					while($rows = $res->fetch_assoc()){
						?>
							<tr>
								<td><?php echo $rows->empname ?></td>
								<td><?php echo $rows->empemail ?></td>
								<td><?php echo $rows->empdob ?></td>
								<td><?php echo $rows->empdept ?></td>
								<td><a href="edit.php?id=<?php echo $rows->employeeid ?>">Edit</a> ~ 
									<a href="delete.php?id=<?php echo $rows->employeeid ?>">Delete</a></td>
							</tr>
						<?php
					}
				} else{
					?>
						<tr>
							<td colspan="5" style="text-align: center; color: red">No records found</td>
						</tr>
					<?php
				}
			?>
		</tbody>
	</table>
</div>
</body>
</html>